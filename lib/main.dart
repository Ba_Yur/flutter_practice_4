import 'package:flutter/material.dart';
import 'package:flutter_practice_4/screens/first.dart';

import 'screens/fourth.dart';
import 'screens/second.dart';
import 'screens/third.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: First.routeName,
      // home: MyHomePage(),
      routes: {
        First.routeName: (ctx) => First(),
        Second.routeName: (ctx) => Second(),
        Third.routeName: (ctx) => Third(),
        Fourth.routeName: (ctx) => Fourth(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text('Appbar'),
      ),
      body: Center(
        child: Container(
          child: Text(
            '1241',
            style: TextStyle(fontSize: 50),
          ),
        ),
      ),
    );
  }
}

void changePage(BuildContext context, String routeName) {
  Navigator.of(context).pushReplacementNamed(routeName);
}

Widget TheDrawer(BuildContext context) {
  return Drawer(
    child: ListView(
      children: [
        Container(
          height: 600,
          margin: EdgeInsets.all(20),
          child: ElevatedButton(
            onPressed: () {
              changePage(context, First.routeName);
            },
            child: Text(
              'Go to first',
              style: TextStyle(
                fontSize: 50,
              ),
            ),
          ),
        ),
        Container(
          height: 600,
          margin: EdgeInsets.all(20),
          child: ElevatedButton(
            onPressed: () {
              changePage(context, Second.routeName);
            },
            child: Text(
              'Go to second',
              style: TextStyle(
                fontSize: 50,
              ),
            ),
          ),
        ),
        Container(
          height: 600,
          margin: EdgeInsets.all(20),
          child: ElevatedButton(
            onPressed: () {
              changePage(context, Third.routeName);
            },
            child: Text(
              'Go to third',
              style: TextStyle(
                fontSize: 50,
              ),
            ),
          ),
        ),
        Container(
          height: 600,
          margin: EdgeInsets.all(20),
          child: ElevatedButton(
            onPressed: () {
              changePage(context, Fourth.routeName);
            },
            child: Text(
              'Go to fourth',
              style: TextStyle(
                fontSize: 50,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
