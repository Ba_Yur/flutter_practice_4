
import 'package:flutter/material.dart';

import '../main.dart';

class Third extends StatelessWidget {

  static const String routeName = '/third';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text('Appbar'),
      ),
      body: Center(
        child: Container(
          child: Text(
            '3',
            style: TextStyle(fontSize: 50),
          ),
        ),
      ),
      drawer: TheDrawer(context),
    );
  }
}
