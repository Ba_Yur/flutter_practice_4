
import 'package:flutter/material.dart';
import 'package:flutter_practice_4/main.dart';

class First extends StatelessWidget {

  static const String routeName = '/first';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text('Appbar'),
      ),
      body: Center(
        child: Container(
          child: Text(
            '1',
            style: TextStyle(fontSize: 50),
          ),
        ),
      ),
      drawer: TheDrawer(context),
    );
  }
}
