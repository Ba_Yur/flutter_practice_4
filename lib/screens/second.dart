
import 'package:flutter/material.dart';

import '../main.dart';

class Second extends StatelessWidget {

  static const String routeName = '/second';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text('Appbar'),
      ),
      body: Center(
        child: Container(
          child: Text(
            '2',
            style: TextStyle(fontSize: 50),
          ),
        ),
      ),
      drawer: TheDrawer(context),
    );
  }
}
