
import 'package:flutter/material.dart';

import '../main.dart';

class Fourth extends StatelessWidget {

  static const String routeName = '/fourth';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text('Appbar'),
      ),
      body: Center(
        child: Container(
          child: Text(
            '4',
            style: TextStyle(fontSize: 50),
          ),
        ),
      ),
      drawer: TheDrawer(context),
    );
  }
}
